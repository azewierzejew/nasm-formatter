import java.io.File
import kotlin.math.log
import kotlin.math.roundToInt
import kotlin.system.exitProcess

val indentNames = listOf(
    "adc", "sbb", "add", "sub",
    "cmp", "cmps", "scas",
    "neg", "dec", "inc",
    "mul", "imul", "div", "idiv",
    "rcl", "rcr", "rol", "ror", "sal", "sar", "shl", "shr", "shld", "shrd",
    "bsf", "bsr", "bt", "bts", "btr", "btc",
    "and", "or", "test",
    "xor", "not", "clc", "cmc", "stc",
    "jo", "jno", "jb", "jnae", "jc", "jnb", "jae", "jnc",
    "je", "jz", "jne", "jnz", "jbe", "jna", "jnbe", "ja", "js", "jns", "jp", "jpe",
    "jnp", "jpo", "jl", "jnge", "jnl", "jge", "jle", "jng", "jnle", "jg", "jrcxz",
    "loop", "loope", "loopz", "loopne", "loopnz",
    "call", "ret", "push", "pop",
    "mov", "movzx",
    "rep", "repz", "repe", "repnz", "repne",
    "scasb", "scasd",
    "syscall"
)

object Formatter {
    @JvmStatic
    fun main(args: Array<String>) {
        if (args.size < 2) {
            println("usage: Formatter indent_size filename")
            exitProcess(1)
        }
        val namesPadding = args[0].toInt()
        val file = File(args[1])
        val lines = file.readLines().map(::toRawLine)
        val names =
            indentNames + lines
                .filter(::hasExtraIndentName)
                .map(::extractIndentMacroName)
                .toList()

        val isIndentLine = getNamesChecker(names)
        val indented = indentNames(lines, isIndentLine, namesPadding)
        val commentsIndented = indentComments(indented)
        val format = "%${log(commentsIndented.size.toDouble(), 10.0).roundToInt() + 1}d: %s"
        val indexer = getIndexedLineFunction(format)
        commentsIndented.mapIndexed(indexer).forEach(::println)
        val code = commentsIndented.joinToString(separator = System.lineSeparator())
        File(file.absolutePath.replace(file.nameWithoutExtension, "${file.nameWithoutExtension}_formatted")).apply {
            setWritable(true)
            writeText(code)
        }
    }
}

fun toRawLine(line: String) = line.replace("\\s+".toRegex(), " ").trim()

fun getIndexedLineFunction(format: String): (Int, String) -> String =
    { index, line -> String.format(format, index + 1, line) }

fun hasExtraIndentName(line: String) = line.startsWith("%macro")

fun getFirstName(line: String) = line.takeWhile { it != ' ' }

fun extractIndentMacroName(line: String) = getFirstName(line.drop("%macro ".length))

fun hasComment(line: String) = line.contains(';')

fun startWithComment(line: String) = line.startsWith(';')

fun commentStart(line: String) = line.indexOf(';')

fun getNamesChecker(list: List<String>): (String) -> Boolean = { line -> list.any { line.startsWith(it) } }

fun indentNames(lines: List<String>, isIndentLine: (String) -> Boolean, namesPadding: Int): List<String> {
    val varNamesIndent = lines
        .filter(isIndentLine)
        .map(::getFirstName)
        .map(String::length).max()

    return varNamesIndent?.let {
        return@let lines.map { line ->
            when {
                isIndentLine(line) -> {
                    val name = getFirstName(line)
                    val tail = line.slice((name.length + 1) until line.length)
                    name.padStart(name.length + namesPadding) + tail.padStart(1 + varNamesIndent - name.length + tail.length)
                }
                else -> line
            }
        }
    } ?: lines
}

fun indentComments(lines: List<String>): List<String> {
    val commentIndent = lines
        .filter(::hasComment)
        .filterNot(::startWithComment)
        .map(::commentStart).max()

    return commentIndent?.let {
        return@let lines.map { line ->
            when {
                hasComment(line) && !startWithComment(line) -> {
                    val index = commentStart(line)
                    val code = line.take(index)
                    val comment = line.takeLast(line.length - index)
                    code + comment.padStart(commentIndent - code.length + comment.length)
                }
                else -> line
            }
        }
    } ?: lines
}

